#!/bin/bash

### Cross-Origin Resource Sharing (CORS)
read -p "Do you want to enable CORS on your ElasticSearch installation?`echo $'\n[Y/n]?'``echo $'\n> '`" answer_cors
if [[ $answer_cors = Y ]] || [[ $answer_cors = y ]] ; then
    printf "Stopping ElasticSearch...\n"
    sudo service elasticsearch stop    
    sleep 1
    
    sudo echo "# Enable CORS
    http.cors.enabled: true
    # http.cors.allow-origin: true
    ### see http://www.oodlestechnologies.com/blogs/How-to-solve-No-Access-Control-Allow-Origin-with-elastic-search#sthash.NSWaMQby.dpuf
    http.cors.allow-origin: "*"
    http.cors.allow-methods: OPTIONS, HEAD, GET, POST, PUT, DELETE
    http.cors.allow-headers: X-Requested-With,X-Auth-Token,Content-Type, Content-Length" >> elasticsearch.yml
    
    printf "CORS has been enabled\n"
    
    printf "Starting ElasticSearch...\n"
    sudo service elasticsearch start
    sleep 5

    sudo service elasticsearch status
fi

### Basic Authorization 

### Afterward this installation, the 'http-basic' directory will be present inside
### the ElasticSearch config directory, like this:
###     ├── elasticsearch -> /etc/elasticsearch
###     ├── elasticsearch.yml
###     ├── http-basic
###     │   └── elasticsearch-http-basic-1.5.1.jar
###     └── logging.yml

read -p "Do you want to secure your ElasticSearch installation with basic authorization?`echo $'\n[Y/n]?'``echo $'\n> '`" answer_basic
if [[ $answer_basic = Y ]] || [[ $answer_basic = y ]] ; then
  	printf "Stopping ElasticSearch...\n"
    sudo service elasticsearch stop
  	sleep 1

  	### From http://markswanderingthoughts.nl/post/92659201600/securing-your-elasticsearch-with-basic-authorization
  	cd /etc/elasticsearch
  	sudo mkdir http-basic
  	cd http-basic
  	### Http Basic Plugin v1.5.1 is compatible with ElasticSearch 1.7.0
  	printf "Running: wget https://github.com/Asquera/elasticsearch-http-basic/releases/download/v1.5.1/elasticsearch-http-basic-1.5.1.jar\n"
    sudo wget "https://github.com/Asquera/elasticsearch-http-basic/releases/download/v1.5.1/elasticsearch-http-basic-1.5.1.jar"
    sleep 1

    cd ..

    read -p "Specify a basic-auth username:`echo $'\n> '`" username
    read -p "Specify a basic-auth password:`echo $'\n> '`" password

    sudo echo "http.basic.enabled: true
    http.basic.user: ${username}
    http.basic.user: ${password}" >> elasticsearch.yml

    printf "Basic-auth has been added with username: ${username} and password: ${password}\n"

    ### Symbolic from /user/share/elasticsearch to ElasticSearch installation at /etc/elasticsearch
    printf "Creating a symbolic link from /user/share/elasticsearch to ElasticSearch installation at /etc/elasticsearch\nThis is necessary for http-basic plugin to work\n"
    sudo mkdir /user
    sudo mkdir /user/share
    sudo ln -s /etc/elasticsearch /user/share/elasticsearch
    # sudo echo "### Symbolic link from /user/share/elasticsearch to ElasticSearch installation at /etc/elasticsearch
    # sudo ln -s /etc/elasticsearch /user/share/elasticsearch" >> ~/.bashrc
    
    source ~/.bashrc      

    printf "Starting ElasticSearch...\n"
  	sudo service elasticsearch start
    sleep 5

    sudo service elasticsearch status
fi