#!/bin/bash

original_working_directory=$(pwd)

### Add scripts directory to ElasticSearch config directory, so it's something like this:
###     ├── elasticsearch -> /etc/elasticsearch
###     ├── elasticsearch.yml
###     ├── logging.yml
###     └── scripts

printf "Adding scripts directory to ElasticSearch configuration directory\n"
cd /etc/elasticsearch
sudo mkdir scripts
printf "Entering scripts directory"


### Add two Groovy scripts to the new directory:
### 1. General comparator scoring script
### 2. Landing page scoring script
### Afterwards, the config directory will look like this
###     ├── elasticsearch -> /etc/elasticsearch
###     ├── elasticsearch.yml
###     ├── logging.yml
###     └── scripts
###         ├── calculate-score-comparator.groovy
###         └── calculate-score-landing-page.groovy

### Add general comparator scoring script
printf "Adding general comparator scoring script: calculate-score-comparator.groovy\n"
sudo touch calculate-score-comparator.groovy
sudo echo '(_source[field] == value) ? boost_factor : ((binding.getVariables().get("base_factor") != null) ? base_factor : 1.0)' >> calculate-score-comparator.groovy

### Add landing page scoring script
printf "Adding landing page scoring script: calculate-score-landing-page.groovy\n"
sudo touch calculate-score-landing-page.groovy
sudo echo '(_source.pageType == "/apps/medhost/templates/landingpage") ? boost_factor : 1.0' >> calculate-score-landing-page.groovy

printf "Finished adding scripts to scripts directory, returning to your original working directory.\nYou may need to wait at least 60 seconds, as ElsticSearch applies them to all data nodes in the cluster before being able to test the new scripts."
cd $original_working_directory
