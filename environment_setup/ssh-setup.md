# SSH To An Ubuntu Server VirtualBox From Mac OS X

#### On Your Ubuntu Server:
1. First, you need to make sure you have openssh-server on your Ubuntu instance. Run:  
    `sudo apt-get install openssh-server`
2. If you change networks a lot, you'll probably want to install a static IP you can always connect to safely to access your Ubuntu Server. To do this, on the Ubuntu Server, edit `/etc/network/interfaces`, and add the following lines:

        auto eth1
        iface eth1 inet static
        address 192.168.56.10
        netmask 255.255.255.0


#### Now on Your Mac:
3. Power down your Ubuntu Server VM
4. Go to Virtual Box -> Preferences -> Network -> Host-only Networks
5. Add a host-only Network
    - vboxnet0
    - If you double-click on vboxnet0, it should have an IPv4 address of 192.168.56.1
    - ![Add host-only network](vboxnet0.png)
6. Go to your VM's Settings -> Network
7. Add Adapter 2
    - Attached to: Host-only Adapter
    - Name: vboxnet0
8. Restart your VM
9. You should now be able to connect to your Ubuntu Server now on 192.168.56.10 from your Mac.  
    `ssh <username>@192.168.56.10`

#### For Use with an ElasticSearch Installation Running on Your Ubuntu Server VM
10. After following these steps and installing ElasticSearch on your Ubuntu Server VM, you should be able to talk to your ElasticSearch installation.
- When connected over SSH, you should be able to stop, start, restart, and get the status of the ElasticSearch installation using the following commands:  
    `sudo service elasticsearch stop`  
    `sudo service elasticsearch start`  
    `sudo service elasticsearch restart`  
    `sudo service elasticsearch status`  

- When connected over SSH, you should also be able to 
    - `curl -X GET 'http://127.0.0.1:9200'`
- From your Mac, you should be able to make requests to the ElasticSearch
    - Using cURL:  
        `curl -X GET '<username>@192.168.56.10:9200'`
    - Using Postman or clients
    - You should see a response similar to the following:

```javascript
{
    "status": 200,
    "name": "Gloom",
    "cluster_name": "elasticsearch",
    "version": {
        "number": "1.7.0",
        "build_hash": "929b9739cae115e73c346cb5f9a6f24ba735a743",
        "build_timestamp": "2015-07-16T14:31:07Z",
        "build_snapshot": false,
        "lucene_version": "4.10.4"
    },
    "tagline": "You Know, for Search"
}
```


