#!/bin/bash

### Add these lines to ElasticSearch config file - elasticsearch.yml
### May need to change the config file's location in this script
### They enable some static and dynamic scripts sandboxed to the Groovy language:
### - enable inline dynamic search scripts for searches
### - enable indexed dynamic scripts for searches
### - enable static file scripts for searches, updates, mappings, plugins, and data aggregations
### - disable dynamic scripts for updates and mappings

################################################################################################

#!/bin/bash

###### Dynamic Scripts ######
### https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-scripting.html

### Enable/Disable dynamic scripts - *** deprecated in favor of fine-grained settings ***
# script.disable_dynamic: false

### Fine-grained script settings
### scripts may be executed only for languages that are sandboxed
# script.inline: sandbox
# script.indexed: sandbox
# script.file: sandbox

### Disable scripting for update and mapping operations, regardless of the script source, 
### for any engine
# script.update: off
# script.mapping: off

### Language-based script settings (precedence over generic settings)
# script.groovy.sandbox.enabled: true
# script.engine.groovy.file.aggs: on
# script.engine.groovy.file.mapping: on
# script.engine.groovy.file.search: on
# script.engine.groovy.file.update: on
# script.engine.groovy.file.plugin: on
# script.engine.groovy.indexed.aggs: on
# script.engine.groovy.indexed.mapping: off
# script.engine.groovy.indexed.search: on
# script.engine.groovy.indexed.update: off
# script.engine.groovy.indexed.plugin: off
# script.engine.groovy.inline.aggs: off
# script.engine.groovy.inline.mapping: off
# script.engine.groovy.inline.search: on
# script.engine.groovy.inline.update: off
# script.engine.groovy.inline.plugin: off

### Stop cluster while making config changes
printf "Stopping ElasticSearch to update script settings\n"
sudo service elasticsearch stop

printf "Modifying ElasticSearch config settings in elasticsearch.yml\n
Sandbox static and dynamic (inline and indexed) scripts to the Groovy language\n
Enable inline dynamic search scripts for searches\n
Enable indexed dynamic scripts for searches\n
Enable static file scripts for searches, updates, mappings, plugins, and data aggregations\n
Disable dynamic scripts for updates and mappings\n"

### Add changes to elasticsearch.yml
sudo echo '###### Dynamic Scripts ######
### https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-scripting.html

### Enable/Disable dynamic scripts - *** deprecated ***
# script.disable_dynamic: false

### Fine-grained script settings
### scripts may be executed only for languages that are sandboxed
script.inline: sandbox
script.indexed: sandbox
script.file: sandbox

### Disable scripting for update and mapping operations, regardless of the script source,
### for any engine
script.update: off
script.mapping: off

### Language-based script settings (precedence over generic settings)
script.groovy.sandbox.enabled: true
script.engine.groovy.file.aggs: on
script.engine.groovy.file.mapping: on
script.engine.groovy.file.search: on
script.engine.groovy.file.update: on
script.engine.groovy.file.plugin: on
script.engine.groovy.indexed.aggs: on
script.engine.groovy.indexed.mapping: off
script.engine.groovy.indexed.search: on
script.engine.groovy.indexed.update: off
script.engine.groovy.indexed.plugin: off
script.engine.groovy.inline.aggs: off
script.engine.groovy.inline.mapping: off
script.engine.groovy.inline.search: on
script.engine.groovy.inline.update: off
script.engine.groovy.inline.plugin: off' >> /etc/elasticsearch/elasticsearch.yml

### Restart cluster after making config changes 
printf "Starting ElasticSearch to apply updates to script settings\b"
sudo service elasticsearch start

### Wait for cluster to start up
sleep 5

### Get cluster status
sudo service elasticsearch status


################################################################################################
### More restrictive settings:

### Fine-grained script settings
### scripts may be executed only for languages that are sandboxed
# script.inline: off
# script.indexed: sandbox
# script.file: sandbox

### Disable scripting for update and mapping operations, regardless of the script source,
### for any engine
# script.update: off
# script.mapping: off

### Language-based script settings (precedence over generic settings)
# script.groovy.sandbox.enabled: true
# script.engine.groovy.file.aggs: on
# script.engine.groovy.file.mapping: on
# script.engine.groovy.file.search: on
# script.engine.groovy.file.update: on
# script.engine.groovy.file.plugin: on
# script.engine.groovy.indexed.aggs: on
# script.engine.groovy.indexed.mapping: off
# script.engine.groovy.indexed.search: on
# script.engine.groovy.indexed.update: off
# script.engine.groovy.indexed.plugin: off
# script.engine.groovy.inline.aggs: off
# script.engine.groovy.inline.mapping: off
# script.engine.groovy.inline.search: off
# script.engine.groovy.inline.update: off
# script.engine.groovy.inline.plugin: off
