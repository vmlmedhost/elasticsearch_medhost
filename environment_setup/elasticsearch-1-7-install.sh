#!/bin/bash

### Install ElasticSearch on Ubuntu 14.04

### Run using:
### sudo bash elastic-search-1-7-install.sh

printf "Going to install ElasticSearch version 1.7.0\n"

original_working_directory=$(pwd)
cd ~

### Install the Elasticsearch recommended Java runtime, Oracle Java.
printf "Running: sudo add-apt-repository ppa:webupd8team/java\n"
sudo add-apt-repository ppa:webupd8team/java

### Update
printf "Running: sudo apt-get update\n"
sudo apt-get update

### Install Oracle Java (JDK) 7 Installer
printf "Running: sudo apt-get install oracle-java7-installer\n"
sudo apt-get install oracle-java7-installer

### Check the Java version
printf "Java version:\n"
java -version

### Download the archive and unzip ir
# printf "Running: \nwget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.7.0.zip\nunzip elasticsearch-1.7.0.zip\n"
# wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.7.0.zip
# sleep 1
# unzip elasticsearch-1.7.0.zip

### Download and install on Ubuntu from the Debian software package
printf "Running: wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.7.0.deb\ndpkg -i elasticsearch-1.7.0.deb\n"
wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.7.0.deb
sleep 1
sudo dpkg -i elasticsearch-1.7.0.deb

### Modify /etc/elasticsearch/elasticsearch.yml
printf "Modifying /etc/elasticsearch/elasticsearch.yml\n"

### Set bind host
printf "Setting network.bind_host to 192.168.0.1\n"
sudo echo "network.bind_host: 192.168.0.1" >> /etc/elasticsearch/elasticsearch.yml

### Disable dynamic scripts
# printf "Setting script.disable_dynamic to true\n"
# sudo echo "script.disable_dynamic: true" >> /etc/elasticsearch/elasticsearch.yml

### Restart ElasticSearch
printf "Running: sudo service elasticsearch restart\n"
sudo service elasticsearch restart
sleep 5

### Get ElasticSearch status
printf "Running: sudo service elasticsearch status\n"
sudo service elasticsearch status

### Wait for ElasticSearch to start up
printf "Waiting for ElasticSearch to start up\n"
sleep 5

### Test ElasticSearch Install
printf "Running: curl -X GET 'http://127.0.0.1:9200'\n"
curl -X GET 'http://127.0.0.1:9200'

### Can also use 'localhost' or get the IP address using ifconfig, and use either of these in place of '127.0.0.1'

### You should see a response similar to the following:
# {
#   "status": 200,
#   "name": "Gloom",
#   "cluster_name": "elasticsearch",
#   "version": {
#     "number": "1.7.0",
#     "build_hash": "929b9739cae115e73c346cb5f9a6f24ba735a743",
#     "build_timestamp": "2015-07-16T14:31:07Z",
#     "build_snapshot": false,
#     "lucene_version": "4.10.4"
#   },
#   "tagline": "You Know, for Search"
# }

### Add aliases to bashrc
printf "You can stop, start, restart, and get the status of the ElasticSearch installation using:\n"
printf "service elasticsearch stop\n"
printf "service elasticsearch start\n"
printf "service elasticsearch restart\n"
printf "service elasticsearch status\n"

printf "Adding aliases for ElasticSearch commands\n"

sudo echo '###ElasticSearch command aliases
alias es-start="sudo service elasticsearch start"
alias es-stop="sudo service elasticsearch stop"
alias es-restart="sudo service elasticsearch restart"
alias es-status="sudo service elasticsearch status"' >> ~/.bashrc

source ~/.bashrc

printf "You can also now start, stop, restart, and get the status of your ElasticSearch cluster with 'es-start', 'es-stop', 'es-restart', and 'es-status'\n"

### Optionally setup CORS and/or Basic Authorization
printf "Returning to original repository directory"
cd $original_working_directory
printf "After SSH is set up, you can run:\n    sudo bash elasticsearch-cors-basic-auth-setup.sh\nto enable Cross-Origin Resource Sharing (CORS) and/or add basic authorization to your ElasticSearch installation\n"

read -p "Enable Cross-Origin Resource Sharing (CORS) and/or Basic Authorization on your ElasticSearch installation?`echo $'\n[Y/n]?'``echo $'\n> '`" answer
if [[ $answer = Y ]] || [[ $answer = y ]] ; then
	sudo bash elasticsearch-cors-basic-auth-setup.sh
fi

### Optionally setup Dynamic Scripts (settings for index scripts written in Groovy)
read -p "Enable dynamic indexed scripts (sandboxed to those written in Groovy)?`echo $'\n[Y/n]?'``echo $'\n> '`" answer
if [[ $answer = Y ]] || [[ $answer = y ]] ; then
	sudo bash elasticsearch-dynamic-script-settings.sh
fi
