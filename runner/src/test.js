var tests = (function () {

	return {
		testSearch: function (indexName) {
            runner.client.search({
                index: indexName,
                body: {
                    query: {
                        filtered: {
                            query: {
                                bool: {
                                    should: [
                                        {
                                            query_string: {
                                                query: "cancer"
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    }
                },
                size: 100
            }).done(function (data) {
                _(data.hits.hits).each(function (hit) {
                    console.log(hit._source.title);
                }).commit();
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        testAC: function (indexName, query) {
            runner.client.search({
                index: indexName,
                body: {
                    "query": {
                        "match": {
                            "title": query
                        }
                    },
                    "size": 100
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
		testPutScript: function () {
            var scriptID = constants.SCRIPT_ID_TITLE_TEST_BOOST;
            var scriptStr = "(_source.title == 'Prostate Cancer Treatment' || _source.title == 'Statistics of Bladder Cancer' || _source.title == 'Is Sugar the Only Cause of Tooth Decay?') ? boost_factor : 1.0";
            this.putScript(scriptID, scriptStr, constants.SCRIPT_LANGUAGE_DEFAULT);
        },
        testBoost: function (indexName) {
            runner.client.search({
                index: indexName,
                body: {
                    query: {
                        function_score: {
                            query: {
                                filtered: {
                                    query: {
                                        bool: {
                                            should: [
                                                {
                                                    query_string: {
                                                        query: "cancer"
                                                    }
                                                }
                                            ]
                                        }
                                    }
                                }
                            },
                            //boost: 1.0,
                            functions: [
                                {
                                    script_score: {
                                        script: '(_source.title == "Is Sugar the Only Cause of Tooth Decay?") ? 10.0 : 1.0'
                                    }
                                }
                            ],
                            boost_mode: 'multiply',
                            score_mode: 'sum'
                        }
                    },
                    size: 100
                }
            }).done(function (data) {
                _(data.hits.hits).each(function (hit) {
                    console.log(hit._source.title);
                }).commit();
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        // Test indexed dynamic script with ID 'indexedCalculateScoreLandingPage'
        testLandingPageBoostDynamicIndexed: function (indexName, queryStr) {
            var query = queryStr || "cancer";
            console.log("Searching: " + queryStr);
            runner.client.search({
                index: indexName,
                body: { 
                   "query": { 
                        "function_score": {
                            "score_mode": "multiply",
                            "boost_mode": "multiply",
                            "query": {
                                "bool": {
                                    "should": [
                                        {
                                            "query_string": {
                                                "query": query
                                            }
                                        }
                                    ]
                                }
                            }, 
                            "functions": [
                                {
                                    "script_score": {
                                        "script_id": SCRIPT_ID_LANDING_PAGE_BOOST,
                                        "lang": constants.SCRIPT_LANGUAGE_LANDING_PAGE_BOOST,
                                        "params": {
                                            "boost_factor": constants.BOOST_FACTOR_LANDING_PAGE_BOOST
                                        }
                                    }
                                }
                            ]
                        } 
                    } 
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        // Test static file script at /scripts/config/indexed-calculate-score-landing-page.groovy
        testLandingPageBoostStatic: function (indexName, queryStr) {
            var query = queryStr || "cancer";
            console.log("Searching: " + queryStr);
            runner.client.search({
                index: indexName,
                body: { 
                   "query": { 
                        "function_score": {
                            "score_mode": "multiply",
                            "boost_mode": "multiply",
                            "query": {
                                "bool": {
                                    "should": [
                                        {
                                            "query_string": {
                                                "query": query
                                            }
                                        }
                                    ]
                                }
                            }, 
                            "functions": [
                                {
                                    "script_score": {
                                        "script_file": constants.SCRIPT_FILE_LANDING_PAGE_BOOST,
                                        "lang": constants.SCRIPT_LANGUAGE_LANDING_PAGE_BOOST,
                                        "params": {
                                            "boost_factor": constants.BOOST_FACTOR_LANDING_PAGE_BOOST
                                        }
                                    }
                                }
                            ]
                        } 
                    } 
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        // Test static file script at /scripts/config/indexed-calculate-score-comparator.groovy
        testComparatorBoostStatic: function (indexName, queryStr, field, value, boostFactor) {
            // (_source[field] == value) ? boost_factor : ((binding.getVariables().get('base_factor') != null) ? base_factor : 1.0)
            var query = queryStr || "cancer";
            var fieldKey = field || "title";
            var keyValue = value || "Prostate Cancer Treatment";
            var factor = boostFactor || 10.0;
            console.log("Searching: " + queryStr);
            runner.client.search({
                index: indexName,
                body: { 
                   "query": { 
                        "function_score": {
                            "score_mode": "multiply",
                            "boost_mode": "multiply",
                            "query": {
                                "bool": {
                                    "should": [
                                        {
                                            "query_string": {
                                                "query": query
                                            }
                                        }
                                    ]
                                }
                            }, 
                            "functions": [
                                {
                                    "script_score": {
                                        "lang": "groovy",
                                        "script_file": constants.SCRIPT_FILE_COOMPARATOR_BOOST,
                                        "params": {
                                            "boost_factor": factor,
                                            "field": fieldKey,
                                            "value": keyValue
                                        }
                                    }
                                }
                            ]
                        } 
                    } 
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        // Test multiple of static file script at /scripts/config/indexed-calculate-score-comparator.groovy
        testComparatorMultipleBoostStatic: function (indexName, queryStr, scriptObj1, scriptObj2) {
            // (_source[field] == value) ? boost_factor : ((binding.getVariables().get('base_factor') != null) ? base_factor : 1.0)
            var query = queryStr || "cancer";
            var script1 = scriptObj1 || {};
            var fieldKey1 = script1.field || "title";
            var keyValue1 = script1.value || "Prostate Cancer Treatment";
            var factor1 = script1.boostFactor || 10.0;
            var script2 = scriptObj2 || {};
            var fieldKey2 = script2.field || "title";
            var keyValue2 = script2.value || "Statistics of Bladder Cancer";
            var factor2 = script2.boostFactor || 15.0;
            console.log("Searching: " + query);
            runner.client.search({
                index: indexName,
                body: { 
                   "query": { 
                        "function_score": {
                            "score_mode": "multiply",
                            "boost_mode": "multiply",
                            "query": {
                                "bool": {
                                    "should": [
                                        {
                                            "query_string": {
                                                "query": query
                                            }
                                        }
                                    ]
                                }
                            }, 
                            "functions": [
                                {
                                    "script_score": {
                                        "lang": "groovy",
                                        "script_file": constants.SCRIPT_FILE_COOMPARATOR_BOOST,
                                        "params": {
                                            "boost_factor": factor1,
                                            "field": fieldKey1,
                                            "value": keyValue1
                                        }
                                    }
                                },
                                {
                                    "script_score": {
                                        "lang": "groovy",
                                        "script_file": constants.SCRIPT_FILE_COOMPARATOR_BOOST,
                                        "params": {
                                            "boost_factor": factor2,
                                            "field": fieldKey2,
                                            "value": keyValue2
                                        }
                                    }
                                }
                            ]
                        } 
                    } 
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            }); 
        },
        // Test inline dynamic file script
        testBoostDynamicInline: function (indexName, queryStr) {
            var query = queryStr || "cancer";
            console.log("Searching: " + queryStr);
            runner.client.search({
                index: indexName,
                body: { 
                   "query": { 
                        "function_score": {
                            "score_mode": "multiply",
                            "boost_mode": "multiply",
                            "query": {
                                "bool": {
                                    "should": [
                                        {
                                            "query_string": {
                                                "query": query
                                            }
                                        }
                                    ]
                                }
                            }, 
                            "functions": [
                                {
                                    "script_score": {
                                        "scripte": "_source.title == 'Prostate Cancer Treatment' ? 100.0 : 1.0"
                                    }
                                }
                            ]
                        } 
                    } 
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        // Create and test indexed dynamic script
        testBoostDynamicIndexed: function (indexName, queryStr) {
            console.log("Putting test script");
            var query = queryStr || "cancer";
            var scriptID = constants.SCRIPT_ID_TITLE_TEST_BOOST;
            var scriptLang = constants.SCRIPT_LANGUAGE_DEFAULT;
            var scriptStr = "(_source.title == 'Prostate Cancer Treatment' || _source.title == 'Statistics of Bladder Cancer' || _source.title == 'Is Sugar the Only Cause of Tooth Decay?') ? boost_factor : 1.0";
            
            var deleteTestScript = function () {
            	runner.client.deleteScript({
	                id: scriptID,
	                lang: scriptLang
	            }).done(function (data) {
                    console.log(data);

                }).fail(function (jqXHR, textStatus) {
                    console.log(jqXHR, textStatus);
                });
            };

            // Create indexed dynamic script
            runner.client.putScript({
                id: scriptID,
                lang: scriptLang,
                body: {
                    script: scriptStr
                }
            }).done(
            	// Test indexed dynamic script
	           	function (data) {
	                console.log(data);
	                runner.client.search({
	                    index: indexName,
	                    body: { 
	                       "query": { 
	                            "function_score": {
	                                "score_mode": "multiply",
	                                "boost_mode": "multiply",
	                                "query": {
	                                    "bool": {
	                                        "should": [
	                                            {
	                                                "query_string": {
	                                                    "query": query
	                                                }
	                                            }
	                                        ]
	                                    }
	                                }, 
	                                "functions": [
	                                    {
	                                        "script_score": {
	                                            "script_id": constants.SCRIPT_ID_TITLE_TEST_BOOST,
	                                            "lang": scriptLang,
	                                            "params": {
	                                                "boost_factor": 10.0
	                                            }
	                                        }
	                                    }
	                                ]
	                            } 
	                        } 
	                    }
	                }).done(function (data) {
	                    console.log(data);
	                    deleteTestScript();
	                }).fail(function (jqXHR, textStatus) {
	                    console.log(jqXHR, textStatus);
	                    deleteTestScript();
	                });
            	}
            ).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        categoryLandingPage: {
            data: {
                iteration: 0,
                exactMatches: [],
                partialMatches: [],
                nonMatches: [],
                failedSearches: [],
                categoriesRemaining: [] 
            },
            test: function (indexName, categories) {
                /**
                 * Functional test for success of category landing page boosting
                 * Loop through all categories
                 * Search for each category title (lowercase without special characters or possessives)
                 * And compare that category's path with the path of the first search result
                 * The two paths should be equal in all or almost all cases
                 * @param {String} indexName index name to test against
                 * @param {Array} categories optional categories list to test, defaults to constants.CATEGORIES
                 */
                var LOOP_INTERVAL_TIME = 7500;
                var MAX_ITERATIONS = 5;
                var ACCEPTABLE_BRANCH_LENGTH_DIFFERENCE = 1;

                var getTestResult = _getTestResult.bind(this);
                var retestRemainingCategories = _retestRemainingCategories.bind(this);

                // empty the failedSearches and categoriesRemaining lists each time the test is run
                _.extend(this.data, {
                    failedSearches: [],
                    categoriesRemaining: []
                });
  
                var exactMatches = this.data.exactMatches;
                var partialMatches = this.data.partialMatches;
                var nonMatches = this.data.nonMatches;
                var failedSearches = this.data.failedSearches;
                var categoriesRemaining = this.data.categoriesRemaining;
                
                var initialExactMatchesCount = exactMatches.length;
                var initialPartialMatchesCount = partialMatches.length;
                var initialNonMatchesCount = nonMatches.length;
                
                var i = 0;
                var categoryArr = categories || constants.CATEGORIES;
                var max = categoryArr.length;

                var getFinishedCount = function () {
                    return exactMatches.length + partialMatches.length + nonMatches.length + 
                        failedSearches.length - initialExactMatchesCount - initialPartialMatchesCount - initialNonMatchesCount;
                };

                // var isTitleEqualToQuery = function (item, queryStr) {
                //  var title = item.title;
                //  if (typeof title !== 'string') {
                //      console.log('No title for item with id: ' + (item.ElasticId || item.id));
                //      title = item.regularTitle;
                //      if (typeof title !== 'string') {
                //          console.log('No `regular title` for item with id: ' + (item.ElasticId || item.id));
                //          return false;
                //      }
                //  }
                //  var titleLower = title.toLowerCase().replace("'s","").replace(/[\.,-\/#!$%\^&\*;\\\:{}|=\-_`'"~()@\+\?><\[\]\+\d]/g, "");
                //  return (titleLower === queryStr);
                // };

                var isPathEqualToQueryPath = function (item, path) {
                    return item.path === path;
                };

                var isPathContainedByOrContainsQueryPath = function (item, queryPath) {
                    var itemPathBranchLength = item.path.split('/').length;
                    var queryPathBranchLength = queryPath.split('/').length;
                    if (Math.abs(itemPathBranchLength - queryPathBranchLength) > ACCEPTABLE_BRANCH_LENGTH_DIFFERENCE) {
                        return false;
                    }
                    if (_.contains(queryPath, item.path)) {
                        return true;
                    }
                    if (_.contains(item.path, queryPath)) {
                        return true;
                    }
                    return false;
                };

                var isLandingPage = function (item) {
                    return item.pageType === constants.PATH_TEMPLATE_LANDING_PAGE;
                };

                var getTestResultString = function (success, resultObj) {
                    var resultStr = (success ? "\nSuccessful: ": "\nNonmatch: ") +
                        "\ntitle: " + resultObj.resultTitle + "\nand path: " + resultObj.resultPath +
                        "\nreturned for query: " + resultObj.queryTitle;
                    return resultStr;
                };

                var getResultObject = function (item, path, queryStr) {
                    var itemSource = item._source;
                    return {
                        queryTitle: queryStr,
                        queryPath: path,
                        resultTitle: itemSource.title,
                        resultPath: itemSource.path,
                        landingPage: isLandingPage(itemSource),
                        score: item._score
                    };
                };

                var checkIfFirstResultMatchesCategoryLandingPage = function (data, path, queryStr) {
                    var firstItem = data[0];
                    if (_.isUndefined(firstItem)) {
                        console.log("No first item for query: " + queryStr);
                        return false;
                    }
                    var firstItemSource = firstItem._source;
                    if (!_.isObject(firstItemSource)) {
                        console.log("No source for first item of query: " + queryStr);
                        return false;
                    }

                    var resultObj = getResultObject(firstItem, path, queryStr);

                    var isMatch = isPathEqualToQueryPath(firstItemSource, path);
                    if (isMatch) {
                        exactMatches.push(resultObj);                   
                        console.log(getTestResultString(isMatch, resultObj));
                        return isMatch;
                    } 

                    var isPartialMatch = isPathContainedByOrContainsQueryPath(firstItemSource, path);
                    if (isPartialMatch) {
                        partialMatches.push(resultObj);
                    } else {
                        nonMatches.push(resultObj);
                        console.warn(getTestResultString(isMatch, resultObj));
                    }       
                    
                    return isPartialMatch;
                };

                var buildBoostedQueryPayload = function (queryStr) {
                    return {
                        "highlight": {
                            "fields": {},
                            "fragment_size": 2147483647,
                            "pre_tags": [
                                "@start-highlight@"
                            ],
                            "post_tags": [
                                "@end-highlight@"
                            ]
                        },
                        "size": 500,
                        "query": {
                            // "custom_filters_score": {
                            //     "query": {
                            //         "query_string": {
                            //             "query": queryStr
                            //         }
                            //     },
                            //     "score_mode": "total",
                            //     "filters": [
                            //         // {
                            //         //     "boost": "2",
                            //         //     "filter": {
                            //         //         "terms": {
                            //         //             "color": [
                            //         //                 "red",
                            //         //                 "yellow"
                            //         //             ]
                            //         //         }
                            //         //     }
                            //         // },
                            //         {
                            //             "boost": "10",
                            //             "filter": {
                            //                 "term": {
                            //                  "pageType": constants.PATH_TEMPLATE_LANDING_PAGE,
                            //                  "_cache" : false
                            //                 }
                            //             }
                            //         }
                            //     ]
                            // }
                            // "function_score": {
                            //     "score_mode": "multiply",
                            //     "boost_mode": "multiply",
                            //     "query": {
                            //         "filtered": {
                            //             "filter": {
                            //                 "bool": {
                            //                     "must": [],
                            //                     "should"
                            //                 }
                            //             },
                            //             "query": {
                            //                 "bool": {
                            //                     "should": [
                            //                         {
                            //                             "query_string": {
                            //                                 "query": queryStr
                            //                             }
                            //                         }
                            //                     ]
                            //                 }
                            //             }
                            //         }
                            //     },
                            //     "functions": [
                            //         {
                            //             "script_score": {
                            //                 // "script_file": constants.SCRIPT_FILE_LANDING_PAGE_BOOST,
                            //                 "script_id": constants.SCRIPT_ID_LANDING_PAGE_BOOST,
                            //                 "lang": "groovy",
                            //                 "params": {
                            //                     "boost_factor": constants.BOOST_FACTOR_LANDING_PAGE_BOOST
                            //                 }
                            //             }
                            //         }
                            //     ]
                            // }
                            "filtered": {
                                "query": {
                                    "function_score": {
                                        "score_mode": "multiply",
                                        "boost_mode": "multiply",
                                        "query": {
                                            "query_string": {
                                                "query": queryStr,
                                                "fields": [
                                                    "title",
                                                    // "regularTitle",
                                                    // "acronymList",
                                                    // "article.*",
                                                    "description",
                                                    "path^10"
                                                ]
                                            }
                                        },
                                        "functions" : [
                                            {
                                                "script_score": {
                                                    "script_id": "indexedCalculateScoreLandingPage",
                                                    "lang": "groovy",
                                                    "params": {
                                                        "boost_factor": 8.0
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                }
                            }
                        },
                        "sort": [
                            {
                                "_score": {
                                    "order": "desc",
                                    "ignore_unmapped": true
                                }
                            }
                        ]
                    };
                };

                function _retestRemainingCategories () {
                    // stop testing if it's has been run too many times already
                    if (this.data.iteration >= MAX_ITERATIONS) {
                        this.data.iteration = 0;
                        console.log("Not testing anymore the categories whose searches failed before.\nThey can be found" +
                            " at: `test.categoryLandingPageTestData.categoriesRemaining`");
                        console.log(categoriesRemaining);
                        console.log(failedSearches);
                        return this.data;                   
                    }

                    console.log("Re-testing categories whose searches failed before...");
                    this.data.iteration++;
                    
                    this.testLandingPageBoostForCategories(indexName, categoriesRemaining);                 
                    
                    return this.data;
                }

                function _getTestResult () {
                    var finished = getFinishedCount();
                    if (finished < max) {
                        return this.data;
                    }

                    var infoStr = "exact matches: " + exactMatches.length.toString() +
                        ", partial matches: " + partialMatches.length.toString() +
                        ", non-matches: " + nonMatches.length.toString() +
                        ", failed searches: " + failedSearches.length.toString();
                    console.info(infoStr);

                    if (categoriesRemaining.length === 0) {
                        return this.data;
                    }       

                    // Re-test remaining categories that had failed before              
                    return retestRemainingCategories();
                }

                var search = function (queryStr, path, index) {
                    console.log("Searching: `" + queryStr + "` for path `" + path + "`");               

                    var payload = buildBoostedQueryPayload(queryStr);
                    
                    runner.client.search({
                        index: indexName,
                        body: payload
                    }).done(function (data) {
                        console.log(data);
                                            
                        var hitsObj = data.hits;
                        if (!_.isObject(hitsObj)) {
                            console.log('No hits for query: ' + queryStr);
                            return false;
                        }

                        var hits = hitsObj.hits;
                        if (!_.isArray(hits)) {
                            console.log('No hits for query: ' + queryStr);
                            return false;
                        }

                        checkIfFirstResultMatchesCategoryLandingPage(hits, path, queryStr);
                        
                        getTestResult();

                        return true;        
                    }).fail(function (jqXHR, textStatus) {
                        console.log(jqXHR, textStatus);

                        var errorObj = {
                            queryTitle: queryStr,
                            queryPath: path,
                            index: index,
                            jqXHR: jqXHR
                        };
                        failedSearches.push(errorObj);

                        var categoryObj = categoryArr[index];
                        categoriesRemaining.push(categoryObj); 

                        getTestResult(); 
                        
                        return false;                                      
                    });
                };
                        
                var loopThroughCategories = function () {
                    setTimeout(function () {
                        console.log('Testing category ' + i.toString());
                        var category = categoryArr[i];

                        search(category.title, category.path, i);                   
                        
                        i++;                    
                        if (i < max) {                      
                            loopThroughCategories();
                        }
                    }, LOOP_INTERVAL_TIME);
                };

                console.log("Running tests for boosted landing/category page results...");
                loopThroughCategories();
            },
            testGetStringAcronymList: function (strTest, acronymListTest) {
                var STR_TEST = "This., -/ \\ is #! an $ % ^ & * | ® © ™ … – — � ¿ º » ⅐ ⅑ ⅒ ⅓ ⅔ ⅕ ⅖ ⅗" +
                    " ⅘ ⅙ ⅚ ⅛ ⅜ ⅝ ⅞ ⅟ ¼ ½ ¾ •  \u00AE  \u00A9  \u2122  \u2013  \u2026  \u2014  \uFFFD  \u2150" +
                    " \u2151 \u2152 \u2153 \u2154 \u2155 \u2156 \u2157 \u2158 \u2159 \u215A \u215B \u215C \u215D" +
                    " \u215E \u215F  \u00BA  \u00BB  \u00BC  \u00BD  \u00BE  \u00BF  \u2022  \u2018  \u2019" +
                    " \u201A  \u201B  \u201C  \u201D  \u201E  \u201F  \u00B1 example ;: {} of a = -_ AIDS" +
                    " string A.I.D.S. with `~)() punctuation 123456789 and an ACRONYM. This is a <tag>asdf" +
                    " qwer</tag>  and only its contents    should \" \' \` be kept. Dr. Jones, MD";
                var ACRONYM_LIST_TEST = ["AIDS", "AIDS", "ACRONYM", "MD"];
                
                var str = strTest || STR_TEST;                      
                console.log("Input string:\n" + str);
                
                var strTransformed = acronymAnalyzer.transformString(str);      
                console.log("Transformed string:\n" + strTransformed);       
                
                var acronymList = acronymListTest || ACRONYM_LIST_TEST;         
                console.log("Input acronym list:");
                console.log(acronymList);
                
                var acronymListResult = acronymAnalyzer.getStringAcronymList(str);
                console.log("Resulting acronym list:");
                console.log(acronymListResult);
                
                var success = true;
                if (acronymList.length !== acronymListResult.length) {
                    success = false;
                    console.warn("Failed to identify acronyms successfully.");              
                }
                
                var anyNotFound = false;            
                var anyNotExpected = false;

                var notFound = [];
                var notExpected = [];

                _.forEach(acronymList, function (word) {
                    if (!_.contains(acronymListResult, word)) {
                        success = false;
                        anyNotFound = true;
                        notFound.push(word);
                    }
                });

                _.forEach(acronymListResult, function (word) {
                    if (!_.contains(acronymList, word)) {
                        success = false;
                        anyNotExpected = true;
                        notExpected.push(word);
                    }
                });

                if (anyNotFound) {
                    console.log("The following expected acronyms were not in the resulting list:");
                    console.log(notFound);
                }

                if (anyNotExpected) {
                    console.log("The following resulting acronyms were not in the expected list:");
                    console.log(notExpected);
                }
                
                if (success) {
                    console.log("Identified all acronyms successfully.");
                }

                return success;
            }            
        }
	};

})();