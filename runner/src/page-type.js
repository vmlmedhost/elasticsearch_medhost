(function () {

	/**
     * Page type module containing analyzer settings, mappings, and methods for handling page types
     */	
	window.pageTypeAnalyzer = {
	// global.pageTypeAnalyzer = {
		
		/**
	     * Analysis settings needed for page type analysis
	     */
		analysis: {},

		/**
	     * Mapping `pageType` field properties within `pages` field properties
	     */
		mapping: {	        
	        "type": "string"
	    }
	};

})();