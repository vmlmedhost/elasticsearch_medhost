(function () {

    /**
     * Path module containing analyzer settings, mappings, and methods for handling acronyms
     */ 
    window.pathAnalyzer = {
    // global.pathAnalyzer = {
        
        /**
         * Analysis settings needed for path analysis
         * Note, each back-slash is escaped with a back-slash for this to be a valid JSON string
         */
        analysis: {         
            "analyzer": {
                "path_index_analyzer": {
                    "type": "custom",
                    "tokenizer": "pattern",
                    "pattern": "/(content\\/krames\\/en\\/article\\/|content\\/medhost\\/your\\-care\\-everywhere\\/)|[\\/\\-\\_]/g",
                    "filter": [
                        "lowercase"
                    ]
                },
                "path_search_analyzer": {
                    "type": "custom",
                    "tokenizer": "pattern",
                    "pattern": "/[\\/\\-\\_]/g",
                    "filter": [
                        "lowercase"
                    ]
                }
            }
        },

        /**
         * Mapping `path` field properties within `pages` field properties
         */
        mapping: {          
            "type": "string",
            "index": "analyzed",
            "index_analyzer": "path_index_analyzer",
            "search_analyzer": "path_search_analyzer",
            "search_quote_analyzer": "path_search_analyzer"
        } 
    };

})();