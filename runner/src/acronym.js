(function () {

	var isWordAcronym = function (word) {
        if (word.length === 0) {
            return false;
        }
        return word.toUpperCase() === word;
    };

    /**
     * Acronym module containing analyzer settings, mappings, and methods for handling acronyms
     */
	window.acronymAnalyzer = {
	// global.acronymAnalyzer = {
		
	    /**
	     * Transform string before separating words into list
	     * @param {str} string
	     */
	    transformString: function (str) {
	        var strTransformed = str
	            // Remove all periods, in case an acronym is separated by periods
	            .replace(/\./g,"")
	            // Remove all punctuation and digits and replace with spaces
	            .replace(/[\.,-\/#!$%\^&\*;\\\:{}|=\-_\`\'\"~()@\+\?><\[\]\+\d\u00AE\u00A9\u2122\u2013\u2026\u2014\uFFFD\u2150\u2151\u2152\u2153\u2154\u2155\u2156\u2157\u2158\u2159\u215A\u215B\u215C\u215D\u215E\u215F\u00BA\u00BB\u00BC\u00BD\u00BE\u00BF\u2022\u00B1\u2018\u2019\u201A\u201B\u201C\u201D\u201E\u201F]/g, " ")
	            // Remove extra whitespace and replace with single spaces
	            .replace(/\s{2,}/g," ")
	            // Remove single-character tokens and extra whitespace
	            .replace(/\b\w{1,1}\b\s?/g, "");
	        return strTransformed;
	    },

	    /**
	     * Get array of acronyms from string
	     * @param {str} string
	     */ 
	    getStringAcronymList: function (str) {  	                 
	        var acronymList = [];
	        if (typeof str !== 'string') {
	            return acronymList;
	        }
	        var strTransformed = this.transformString(str);
	        var wordList = strTransformed.split(" ");
	        for (var i = 1, j = wordList.length; i < j; i++) {
	            var word = wordList[i];
	            if (isWordAcronym(word)) {
	                acronymList.push(word);
	            }
	        }
	        return acronymList;
	    },        

	    /**
         * Analysis settings needed for acronym analysis
         */
	    analysis: {	       
	        "filter": {
	            "acronym_stop_words": {
	                "type": "stop",
	                "stopwords": "_english_"
	            }
	        },
	        "analyzer": {
	            "acronym_index_analyzer": {
	                "type": "custom",
	                "tokenizer": "classic",
	                "filter": [
	                    "classic",
	                    "asciifolding",
	                    "acronym_stop_words"
	                ]
	            },
	            "acronym_search_analyzer": {
	                "type": "custom",
	                "tokenizer": "classic",
	                "filter": [
	                    "classic",
	                    "asciifolding",
	                    "acronym_stop_words"
	                ]
	            }
	        }
	    },        

	    /**
         * Mapping `acronymList` field properties within `pages` field properties
         */
	    mapping: {	        
	        "type": "string",
	        "index_analyzer": "acronym_index_analyzer",
	        "search_analyzer": "acronym_search_analyzer"
	    }
	};

})();
