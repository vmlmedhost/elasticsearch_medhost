var runner = (function () {
    var client = new $.es.Client({
       hosts: [
           // constants.HOST_PROD
           // constants.HOST_DEV
           // constants.HOST_VM_LOCAL
           constants.HOST_VM_PRIVATE
        ]
    });

    var filter = function (keywords) {
        return _(keywords).without('&amp').map(function (keyword) {
            return keyword.toLowerCase();
        }).value();
    };

    var getAcronyms = function (page) {
        var getData = function (page) {
            return {
                title: page._source.title,
                desc: page._source.description,
                text: page._source.article &&
                    $('<p>' + page._source.article.text + '</p>').text(),
                onlineSourcesNameList:
                    _(page._source.OnlineSources).map(function (source) {
                        return source.websiteName;
                    }).value()
            };
        };

        var acronyms = [];
        var data = getData(page);

        acronyms.push.apply(acronyms, acronymAnalyzer.getStringAcronymList(data.title));
        acronyms.push.apply(acronyms, acronymAnalyzer.getStringAcronymList(data.desc));
        acronyms.push.apply(acronyms, acronymAnalyzer.getStringAcronymList(data.text));

        if (data.onlineSourcesNameList.length === 0) {
            return acronyms;
        }

        for (var i = 1, j = data.onlineSourcesNameList.length; i < j; i++) {
            acronyms.push.apply(acronyms, acronymAnalyzer.getStringAcronymList(data.onlineSourcesNameList[i]));
        }

        return acronyms;
    };

    var getKeywords = function (page) {
        var getData = function (page) {
            return {
                title: page._source.title,
                desc: page._source.description,
                text: page._source.article &&
                    $('<p>' + page._source.article.text + '</p>').text(),
                onlineSourcesNameList:
                    _(page._source.OnlineSources).map(function (source) {
                        return source.websiteName;
                    }).value()
            };
        };

        var data = getData(page);
        var keywords = [];

        if (!_(data.title).isUndefined()) {
            Retext().use(retextPOS, {maximum: 10}).process(data.title, function (err, file) {
                if (err) {
                    console.log(err);
                }
                var space = file.namespace('retext');
                space.keywords.forEach(function (keyword) {
                    keywords.push(nlcstToString(keyword.matches[0].node));
                });
            });
        }

        if (!_(data.desc).isUndefined()) {
            Retext().use(retextPOS, {maximum: 10}).process(data.desc, function (err, file) {
                if (err) {
                    console.log(err);
                }
                var space = file.namespace('retext');
                space.keywords.forEach(function (keyword) {
                    keywords.push(nlcstToString(keyword.matches[0].node));
                });
            });
        }

        if (!_(data.text).isUndefined()) {
            Retext().use(retextPOS, {maximum: 30}).process(data.text, function (err, file) {
                if (err) {
                    console.log(err);
                }
                var space = file.namespace('retext');
                space.keywords.forEach(function (keyword) {
                    keywords.push(nlcstToString(keyword.matches[0].node));
                });
            });
        }

        if (!_(data.onlineSourcesNameList).isEmpty()) {
            _(data.onlineSourcesNameList).each(function (name) {
                Retext().use(retextPOS, {maximum: 10}).process(name, function (err, file) {
                    if (err) {
                        console.log(err);
                    }
                    var space = file.namespace('retext');
                    space.keywords.forEach(function (keyword) {
                        keywords.push(nlcstToString(keyword.matches[0].node));
                    });
                });
            });
        }
        console.log(keywords);

        return filter(keywords);
    };

    return {
        client: client,
        getHosts: function (params) {
            // optional params
            // {number: 0, string: true}
            if (!params) {
                return client.transport._config.hosts;
            }
            if (params.number && typeof params.number === 'number') {
                return client.transport._config.hosts[params.number];
            }
            if (params.string) {
                return client.transport._config.hosts.toString().replace(/\,/g, " ");
            }
            return client.transport._config.hosts; 
        },
        createIndex: function (indexName, settings) {
            var payload = {
                index: indexName
            };
            if (settings) {
                payload.body = {
                    settings: settings
                };
            }
            client.indices.create(
                payload
            ).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        deleteIndex: function (indexName) {
            client.indices.delete({
                index: indexName
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        closeIndex: function (indexName) {
            client.indices.close({
                index: indexName
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        openIndex: function (indexName) {
            client.indices.open({
                index: indexName
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        getSettings: function (indexName) {
            client.indices.getSettings({
                index: indexName           
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        putSettings: function (indexName, settings) {            
            /**
             * @param {String} indexName
             * @param {Object} settings, e.g., {
             *     "analysis": {
             *         "filter": {
             *             ...
             *         },
             *         "analyzer": {
             *             ...
             *         }
             *     }
             * }
             */
            client.indices.putSettings({
                index: indexName,
                body: settings                
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        getFieldMapping: function (indexName, field) {
            /**            
             * @param {String} indexName index name
             * @param {String} field field name
             */
            client.indices.getFieldMapping({
                index: indexName,
                field: field
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        getMappings: function (indexName) {
            client.indices.getMapping({
                index: indexName
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        }, 
        putMappings: function (indexName, type, mappings) {           
            /**
             * @param {String} indexName
             * @param {String} type, e.g., "pages"
             * @param {Object} mappings, e.g., {
             *     "pages": {
             *         "properties": {
             *             "title": {
             *                 "type": "string"
             *             }   
             *         }
             *     }
             * }
             */
            client.indices.putMapping({
                index: indexName,
                type: type,
                body: mappings
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },          
        putScript: function (scriptID, scriptString, scriptLang) {            
            /**
             * @param {String} scriptID (to be used as value for "script_id" in queries)
             * @param {String} scriptString (contains the script code)
             * @param {String} scriptLang (defaults to "groovy")
             */
            var lang = scriptLang || constants.SCRIPT_LANGUAGE_DEFAULT;
            client.putScript({
                id: scriptID,
                lang: lang,
                body: {
                    script: scriptString
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        deleteScript: function (scriptID, scriptLang) {
            var lang = scriptLang || constants.SCRIPT_LANGUAGE_DEFAULT;
            client.deleteScript({
                id: scriptID,
                lang: lang
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        getScript: function (scriptID, scriptLang) {
            var lang = scriptLang || constants.SCRIPT_LANGUAGE_DEFAULT;
            client.getScript({
                id: scriptID,
                lang: lang
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        updateScript: function (scriptID, scriptString, scriptLang) {            
            /**
             * DELETE script with ID and PUT updated script with ID
             * @param {String} scriptID (to be used as value for "script_id" in queries)
             * @param {String} scriptString (contains the script code)
             * @param {String} scriptLang (defaults to "groovy")
             */
            var lang = scriptLang || constants.SCRIPT_LANGUAGE_DEFAULT;
            
            var putScript = function () {
                client.putScript({
                    id: scriptID,
                    lang: lang,
                    body: {
                        script: scriptString
                    }
                }).done(function (data) {
                    console.log(data);
                }).fail(function (jqXHR, textStatus) {
                    console.log(jqXHR, textStatus);
                });
            };

            client.deleteScript({
                id: scriptID,
                lang: lang
            }).done(function (data) {
                console.log(data);
                putScript();
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
                putScript();
            });            
        },         
        putAcronymSettings: function (indexName) {
            client.indices.putSettings({
                index: indexName,
                body: {
                    "analysis": acronymAnalyzer.analysis
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },  
        putPathSettings: function (indexName, ignoreConflicts) {            
            client.indices.putSettings({
                index: indexName,
                body: {
                    "analysis": pathAnalyzer.analysis                    
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },  
        createACIndex: function (indexName) {
            client.indices.create({
                index: indexName,
                body: {
                    "settings": {
                        "number_of_shards": 1,
                        "analysis": {
                            "filter": {
                                "autocomplete_filter": {
                                    "type": "edge_ngram",
                                    "min_gram": 1,
                                    "max_gram": 20
                                }
                            },
                            "analyzer": {
                                "autocomplete": {
                                    "type": "custom",
                                    "tokenizer": "standard",
                                    "filter": [
                                        "lowercase",
                                        "autocomplete_filter"
                                    ]
                                }
                            }
                        }
                    }
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        putFieldMapping: function (indexName, fieldName, fieldMapping, ignoreConflicts) {
            /**
             * Add mappings to `pages` document type for:
             * `suggest` (autocomplete array) field
             * `acronymList` (uppercase acronym list) field
             * `pageType` (CQ template path) field
             * @param {String} indexName index name
             * @param {String} fieldName field name (under `pages.properties`)
             * @param {Object} fieldMapping field mapping settings
             * @param {Boolean} ignoreConflicts defaults to true, do not ignore merge conflicts if false
             */            
            if (typeof indexName !== 'string') {
                console.error("You must declare the index name.");
                return false;
            }
            if (typeof fieldName !== 'string') {
                console.error("You must declare the field name you're adding mappings for.");
                return false;
            }
            if (typeof fieldMapping !== 'object') {
                console.error("You must declare the mapping settings object (under `pages.properties`)" +
                " for the field: " + fieldName + ".");
                return false;
            }
            
            var ignore = (ignoreConflicts === false) ? false : true;
            var body = {
                "pages": {
                    "properties": {}
                }
            };
            body.pages.properties[fieldName] = fieldMapping;
            
            console.log("Adding mappings:\n" + JSON.stringify(body.pages.properties) + "\nfor field: " +
                fieldName + ", at `pages.properties." + fieldName + "`, in index: " + indexName);            
            
            if (ignore) {
                console.log("Ignoring potential mapping conflicts.");
            }            
            
            client.indices.putMapping({
                ignoreConflicts: ignore,
                index: indexName,
                type: "pages",
                body: body
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        putACMapping: function (indexName) {
            // client.indices.putMapping({
            //     index: indexName,
            //     type: 'pages',
            //     body: {
            //         "pages": {
            //             "properties": {
            //                 "title": {
            //                     "type": "string",
            //                     "index_analyzer": "autocomplete",
            //                     "search_analyzer": "standard"
            //                 }
            //             }
            //         }
            //     }
            // }).done(function (data) {
            //     console.log(data);
            // }).fail(function (jqXHR, textStatus) {
            //     console.log(jqXHR, textStatus);
            // });
            var mapping = {
                "type": "string",
                "index_analyzer": "autocomplete",
                "search_analyzer": "standard"
            };
            this.putFieldMapping(indexName, "title", mapping);
        },
        putSuggestMapping: function (indexName) {            
            /**
             * Add mapping to `pages` document type for:
             * `suggest` autocomplete list field
             * @param {String} indexName index name
             */
            var mapping = {
                "type": "completion",
                "analyzer": "simple",
                "search_analyzer": "simple"
                //"payloads": true
            };
            this.putFieldMapping(indexName, "suggest", mapping);
        },
        putAcronymMapping: function (indexName) {
            /**
             * Add mapping to `pages` document type for:
             * `acronymList' uppercase acronym list field
             * @param {String} indexName index name
             */
             this.putFieldMapping(indexName, "acronymList", acronymAnalyzer.mapping);
        },
        putPathMapping: function (indexName) {
            /**
             * Update mapping in `pages` document type for:
             * `path' document path field
             * @param {String} indexName index name
             */
            this.putFieldMapping(indexName, "path", pathAnalyzer.mapping);
        },
        putPageTypeMapping: function (indexName) {            
            /**
             * Add mapping to `pages` document type for:
             * `pageType` CQ template path field
             * @param {String} indexName index name
             */
            this.putFieldMapping(indexName, "pageType", pageTypeAnalyzer.mapping);
        },
        putAdditionalTypeMappings: function (indexName, ignoreConflicts) {
            /**
             * Add mappings to `pages` document type for:
             * `suggest` autocomplete array field
             * `acronymList` uppercase acronym list field
             * `pageType` CQ template path field
             * @param {String} indexName index name
             * @param {Boolean} ignoreConflicts defaults to true, do not ignore merge conflicts if false
             */
            var ignore = (ignoreConflicts === false) ? false : true;
            client.indices.putMapping({
                ignoreConflicts: ignore,
                index: indexName,
                type: 'pages',
                body: {
                    "pages": {
                        "properties": {
                            "suggest": {
                                "type": "completion",
                                "analyzer": "simple",
                                "search_analyzer": "simple"
                                //"payloads": true
                            },
                            "acronymList": acronymAnalyzer.mapping,
                            "path": pathAnalyzer.mapping,
                            "pageType": pageTypeAnalyzer.mapping                           
                        }
                    }
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },        
        createLandingPageIndexedDynamicScript: function () {           
            var scriptStr = "(_source.pageType == '" + constants.PATH_TEMPLATE_LANDING_PAGE + "') ? boost_factor: 1.0";
            
            var putScriptLog = "Adding script '" + constants.SCRIPT_ID_LANDING_PAGE_BOOST + "' to" +
                " ElasticSearch cluster at:\n" + this.getHosts({string: true}) + "\nscript: " + scriptStr;        
            console.log(putScriptLog);
            
            this.putScript(constants.SCRIPT_ID_LANDING_PAGE_BOOST, scriptStr, constants.SCRIPT_LANGUAGE_LANDING_PAGE_BOOST);
        },
        createCategoryPageIndexedDynamicScript: function () {         
            var scriptStr = "(_source.pageType == '" + constants.SCRIPT_ID_CATEGORY_PAGE_BOOST + "') ? boost_factor: 1.0";        
            
            var update = "Adding script '" + constants.SCRIPT_ID_LANDING_PAGE_TEST_BOOST + "' to" +
                " ElasticSearch cluster at:\n" + this.getHosts({string: true}) + "\nscript: " + scriptStr;
            console.log(update);
            
            this.updateScript(constants.SCRIPT_ID_LANDING_PAGE_BOOST, scriptStr, constants.SCRIPT_LANGUAGE_LANDING_PAGE_BOOST);
        },
        setupLandingPageBoostDynamic: function (indexName) {
            this.putPageTypeMapping(indexName);
            this.createLandingPageIndexedDynamicScript();
        },
        scroll: function (indexName, args) {
            /**
             * Scroll through all documents in index
             * Scan each document and add:
             * - list of key terms found in title, description, article text, and onlineSources fields
             * - list of acronyms found in title, description, article text, and onlineSources fields
             * Optionally add pageType (for landing pages) and delete duplicate documents. These two are not the
             * defaults, and should NOT be used in production indices
             * @param {String} indexName
             * @param {Object} args optional { assignPageType: true, deleteDuplicates: false }
             */
            var sum = 0;
            var assignPageType = (args || {}).assignPageType || false;
            var deleteDuplicates = (args || {}).deleteDuplicates || false;            
            var scannedPaths = [];
            var categoryPaths = [];            
            if (assignPageType || deleteDuplicates) {
                categoryPaths = JSON.parse(constants.CATEGORY_PATHS);
            }
            if (assignPageType) {
                console.info("Adding page types");
            }
            if (deleteDuplicates) {
                console.info("Deleting duplicate pages");
            }            

            var processPage = function (page) {
                console.log(page._id);
                // console.log(page);
                
                var docObject = {};
                
                // docObject.suggest = {
                //     input: getKeywords(page)
                //     //output: "Nirvana - Nevermind",
                //     //payload: {}
                //     //weight: 34
                // };

                // var acronymList = getAcronyms(page);                
                // if (acronymList.length > 0) {
                //     console.log(acronymList);
                //     // As array:
                //     docObject.acronymList = acronymList;
                //     // Or, as string:
                //     // docObject.acronymList = acronymList.toString().replace(/\,/g, " ");
                // }

                // if (!deleteDuplicates && assignPageType && !_.has(page._source, 'contentFilterType')) {
                //     _.forEach(categoryPaths, function (link) {
                //         if (page._source.path === link && !_.has(page._source, 'pageType')) {
                //             console.log(link);
                //             docObject.pageType = constants.PATH_TEMPLATE_LANDING_PAGE;
                //         }
                //     });            
                // }
                
                if (deleteDuplicates && !_.has(page._source, 'contentFilterType')) {                    
                    var deleted = false;

                    _.forEach(categoryPaths, function (link) {
                        if (page._source.path === link) {            
                            if (_.contains(scannedPaths, page._source.path)) {
                                console.warn("Deleting: " + page._source.path);
                                // console.warn("Deleting: " + page._source.path);
                                // Delete duplicate landing page
                                client.delete({
                                    index: indexName,
                                    type: 'pages',
                                    id: page._id
                                }).fail(function (jqXHR, textStatus) {
                                    console.log(jqXHR, textStatus);
                                });
                                deleted = true;
                                return false;
                            }

                            console.log("Scanned path: "  + page._source.path);
                            scannedPaths.push(page._source.path);

                            // Add page type if it doesn't have it
                            if (assignPageType && page._source.path === link && !_.has(page._source, 'pageType')) {
                                console.info("Adding landing-page pageType to: " + page._source.path);
                                docObject.pageType = constants.PATH_TEMPLATE_LANDING_PAGE;
                            }

                            return false;
                        }
                    });   

                    if (deleted) {
                        return;
                    }         
                }
                
                client.update({
                    index: indexName,
                    type: 'pages',
                    id: page._id,
                    body: {
                        doc: docObject
                    }
                }).fail(function (jqXHR, textStatus) {
                    console.log(jqXHR, textStatus);
                });
            };

            console.log('Scanning document ' + (sum + 1).toString());

            client.search({
                index: indexName,
                scroll: constants.SCROLL_TIME,
                search_type: 'scan'
            }).done(function getMoreUntilDone(data) {
                _(data.hits.hits).each(processPage).commit();

                sum += data.hits.hits.length;
                // if (sum >= data.hits.total) {
                if (sum >= 20) {
                    console.log('Finished scanning and updating all ' + data.hits.hits.length.toString() + ' documents');
                    console.log('----------- DONE -----------');
                    return;
                }

                client.scroll({
                    scrollId: data._scroll_id,
                    scroll: constants.SCROLL_TIME
                }).done(getMoreUntilDone
                ).fail(function (jqXHR, textStatus) {
                    console.log(jqXHR, textStatus);
                });
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        scrollAndRemoveTeasers: function (indexName, args) {
            var sum = 0;           
            var matches = [];
            var matchingIds = [];

            var processPage = function (page) {
                console.log(page._id);

                _.forEach(constants.TEASER_PROPERTIES, function (prop) {
                    var segments = prop.split('.');
                    var segmentLength = segments.length;
                    var parentObj = page._source;                    
                    var childProp = segments[segmentLength - 1];
                    
                    if (segmentLength > 1) {                                              
                        for (var i = 0; i < segmentLength - 1; i++) {
                            var segment = segments[i];
                            if (!_.has(parentObj, segment)) {
                                return true;
                            }
                            parentObj = parentObj[segment];                            
                        }
                    }

                    if (_.has(parentObj, childProp)) {
                        console.info(page);
                        matches.push(page);
                        matchingIds.push(page._id);
                        client.delete({
                            index: indexName,
                            type: 'pages',
                            id: page._id
                        }).done(function (data) {
                            console.log(data);
                        }).fail(function (jqXHR, textStatus) {
                            console.log(jqXHR, textStatus);
                        });
                        return false;
                    }
                });
                
            };

            client.search({
                index: indexName,
                scroll: constants.SCROLL_TIME,
                search_type: 'scan'
            }).done(function getMoreUntilDone(data) {
                _(data.hits.hits).each(processPage).commit();

                sum += data.hits.hits.length;
                if (sum >= data.hits.total) {
                // if (sum >= 20) {                    
                    console.log('Finished scanning all documents.');
                    console.info('Delete documents with ids: ' + matchingIds.toString().replace(/,/g, ", "));
                    console.log(matches);
                    console.log(JSON.stringify(matches));

                    console.log('----------- DONE -----------');
                    return;
                }

                client.scroll({
                    scrollId: data._scroll_id,
                    scroll: constants.SCROLL_TIME
                }).done(getMoreUntilDone
                ).fail(function (jqXHR, textStatus) {
                    console.log(jqXHR, textStatus);
                });
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        suggest: function (indexName, query) {
            client.suggest({
                index: indexName,
                body: {
                    keywordSuggest: {
                        text: query,
                        completion: {
                            field: 'suggest'
                        }
                    }
                }
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        search: function (indexName, query) {
            client.search({
                index: indexName,
                body: query
            }).done(function (data) {
                console.log(data);
            }).fail(function (jqXHR, textStatus) {
                console.log(jqXHR, textStatus);
            });
        },
        setupAcronymAnalysis: {
            indexNameNew: "",
            getIndexName: function (idxNew) {
                if (idxNew) {
                    this.indexNameNew = idxNew;
                    return idxNew;
                }
                if (this.indexNameNew) {
                    return this.indexNameNew;
                }
                console.error("You must specify an index name with a string");
                return "";
            },
            step1: function (idxNew) {
                var idx = this.getIndexName(idxNew);
                if (!idx) {
                    return false;
                }
                /**
                 * Elasticdump
                 * Run elasticdump script - create new index {indexNameNew} and copy
                 * only the mappings (not data) from existing index
                 */
                var alertMapping = "\nYou must run the elasticdump script - create new index '" + idx +
                    "' in cluster:\n" + this.getHosts({string: true}) +
                    "\nand copy only the mappings (not data) from the existing index." +
                    "\nAfterwards, run:\nrunner.setupAcronymAnalysis.step2('" + idx + "')";
                // setTimeout(function () {
                //     alert(alertMapping);
                // }, 1);
                console.info(alertMapping);
                return true;
            },
            step2: function (idxNew) {
                var idx = this.getIndexName(idxNew);
                if (!idx) {
                    return false;
                }
                // Close index {indexNameNew}
                console.log('Closing index ' + idx + ".\nRunning runner.closeIndex(" + idx+ ")");
                client.indices.close(idx)
                    .done(function (data) {
                        console.log(data);
                        // Add analysis settings to {indexNameNew}
                        console.log('Adding acronym analysis settings to index ' + idx + ".\nRunning runner.putAcronymSettings(" + idx+ ")");
                        runner.putAcronymSettings(idx);

                        // Add mappings to 'acronymList' field
                        setTimeout(function () {
                            console.log('Adding acronymList mapping to ' + idx + ".\nRunning runner.putAcronymMapping(" + idx+ ")");
                            runner.putAcronymMapping(idx);
                        }, 500);

                        // Open {indexNameNew}
                        setTimeout(function () {
                            console.log('Opening index ' + idx + ".\nRunning runner.openIndex(" + idx+ ")");
                            client.indices.open(idx)
                                .done(function (data) {
                                    console.log(data);
                                })
                                .fail(function (jqXHR, textStatus) {
                                    console.log(jqXHR);
                                    console.log(textStatus);
                                });
                        }, 500);
                    })
                    .fail(function (jqXHR, textStatus) {
                        console.log(jqXHR);
                        console.log(textStatus);
                    });
                                    
                /**
                 * Elasticdump
                 * Run elasticdump script - copy only the data (not the mappings)
                 * from existing index to index {indexNameNew}
                 */
                setTimeout(function () {
                    var alertData = "\nYou must run the elasticdump script - copy only the data (not the mappings) from" +
                        " the existing index to the new index '" + idx + "' in cluster:\n" +
                        runner.getHosts({string: true}) +
                        "\nAfterwards, run\nrunner.setupAcronymAnalysis.step3('" + idx + "')";
                    // setTimeout(function () {
                    //     alert(alertData);
                    // }, 1);
                    console.info(alertData);
                }, 2000);
                return true;
            },
            step3: function (idxNew) {
                var idx = this.getIndexName(idxNew);
                if (!idx) {
                    return false;
                }
                /**
                 * Scroll through data in {indexNameNew} and add list of acronyms to
                 * 'acronymList' field
                 */
                console.log("Scrolling through data in " + idx + "..." + ".nRunning runner.scroll(" + idx+ ")");
                runner.scroll(idx);                
                return true;
            }
        },
        setupPathAnalysis: function (indexName) {
            // Close index {indexName}
            console.log('Closing index ' + idx + ".\nRunning runner.closeIndex(" + idx+ ")");
            client.indices.close(idx)
                .done(function (data) {
                    console.log(data);
                    // Add analysis settings to {indexName}
                    console.log('Adding path settings to index ' + idx + ".\nRunning runner.putPathSettings(" + idx+ ")");
                    runner.putPathSettings(idx);

                    // Add mappings to 'acronymList' field
                    setTimeout(function () {
                        console.log('Adding path mapping to ' + idx + ".\nRunning runner.putPathMapping(" + idx+ ")");
                        runner.putPathMapping(idx);
                    }, 500);

                    // Open {indexName}
                    setTimeout(function () {
                        console.log('Opening index ' + idx + ".\nRunning runner.openIndex(" + idx+ ")");
                        client.indices.open(idx)
                            .done(function (data) {
                                console.log(data);
                            })
                            .fail(function (jqXHR, textStatus) {
                                console.log(jqXHR);
                                console.log(textStatus);
                            });
                    }, 500);
                })
                .fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log(textStatus);
                });
                                
            /**
             * Elasticdump
             * Run elasticdump script - copy only the data (not the mappings)
             * from existing index to index {indexNameNew}
             */
            setTimeout(function () {
                var alertData = "\nYou must run the elasticdump script - copy only the data (not the mappings) from" +
                    " the existing index to the new index '" + idx + "' in cluster:\n" +
                    runner.getHosts({string: true});
                // setTimeout(function () {
                //     alert(alertData);
                // }, 1);
                console.info(alertData);
            }, 2000);
            return true;
        }
    };
})();
