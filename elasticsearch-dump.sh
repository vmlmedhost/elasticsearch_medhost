#!/bin/bash 

### Uses elasticdump node module
### https://github.com/taskrabbit/elasticsearch-dump

SOURCE_PROTOCOL="<https or http>"
SOURCE_ADDRESS="<cluster address or ip address>"
SOURCE_PORT="<port number>"
SOURCE_USERNAME="<username or client id/key>"
SOURCE_PASSWORD="<password or client secret, can be an empty string>"
SOURCE_INDEX="<index name>"

TARGET_PROTOCOL="<https or http>"
TARGET_ADDRESS="<cluster address or ip address>"
TARGET_PORT="<port number>"
TARGET_USERNAME="<username or client id/key>"
TARGET_PASSWORD="<password or client secret, can be an empty string>"
TARGET_INDEX="<index name>";


if [[ ! "${SOURCE_PASSWORD}" = "" ]]; then	            
	source_path=${SOURCE_PROTOCOL}://${SOURCE_USERNAME}:${SOURCE_PASSWORD}@${SOURCE_ADDRESS}:${SOURCE_PORT}
else
	source_path=${SOURCE_PROTOCOL}://${SOURCE_USERNAME}@${SOURCE_ADDRESS}:${SOURCE_PORT}
fi

if [[ ! "${TARGET_PASSWORD}" = "" ]]; then
	target_path=${TARGET_PROTOCOL}://${TARGET_USERNAME}:${TARGET_PASSWORD}@${TARGET_ADDRESS}:${TARGET_PORT}
else
	target_path=${TARGET_PROTOCOL}://${TARGET_USERNAME}@${TARGET_ADDRESS}:${TARGET_PORT}
fi

source_path_index=${source_path}/${SOURCE_INDEX}
target_path_index=${target_path}/${TARGET_INDEX}


### Copy Mappings
read -p "Copy mappings from:`echo $'\n\"'`${source_path_index}\"`echo $'\nto:\n\"'`${target_path_index}\"?`echo $'\n[Y/n]?'``echo $'\n> '`" answer
if [[ $answer = Y ]] || [[ $answer = y ]] ; then
  	printf "Copying mappings from \"${source_path_index}\" to \"${target_path_index}\"...\n"
	elasticdump \
	  --input=${source_path_index} \
	  --output=${target_path_index} \
	  --type=mapping
fi


### Copy Data
read -p "Copy data from:`echo $'\n\"'`${source_path_index}\"`echo $'\nto:\n\"'`${target_path_index}\"?`echo $'\n[Y/n]?'``echo $'\n> '`" answer
if [[ $answer = Y ]] || [[ $answer = y ]] ; then
  	printf "Copying data from \"${source_path_index}\" to \"${target_path_index}\"...\n"
	elasticdump \
	  --input=${source_path_index} \
	  --output=${target_path_index} \
	  --type=data
fi



### Copy Mappings
# printf "Copying mappings from \"${source_path}\"\nto \"${target_path}\"\n"

# elasticdump \
#   --input=${source_path_index} \
#   --output=${target_path_index} \
#   --type=mapping


### Copy Data 
# printf "Copying data from \"${source_path}\"\nto \"${target_path}\"\n"

# elasticdump \
#   --input=${source_path_index} \
#   --output=${target_path_index} \
#   --type=data
