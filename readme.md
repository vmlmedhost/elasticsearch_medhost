# ElasticSearch API calls and documentation

- *Note: currently in progress*    

### Included:
- For ElasticSearch installation on a Linux server see, 'local_setup'
- Configurable bash script to copy ElasticSearch mapping and/or data between cluster indices (using the elasticdump npm module), see 'elsticsearch-dump.sh'
- Bash script for enabling Cross-Origin Resource Sharing (CORS) and installing/enabling Basic HTTP Authorization
- Bash scripts for adjusting ElasticSearch static, dynamic inline, and dynamic indexed script settings
- For ElasticSearch static scripts written in Groovy, see 'scripts'
- For Bash script for installing ElasticSearch static scripts written in Groovy:
    - general comparator scoring script
    - landing page scoring script
- For collections importable by the Postman advanced REST client, see 'post_man_collections'


### Setting up a new index
- To re-index data, it is easiest to create a new index, copy/add settings, copy/add mappings, and then copy data from the existing index (or run a replication to get all the data currently on your site).
- Process for the Medhost YCE Prod environment:
    1. Setup environments/indices in elasticsearch-dump and run the script copy mappings from `yce` index to `yce_backup` (this index will be created automatically).
    2. Once you're sure everything copied correctly and completely, make a `DELETE` request to delete the `yce` index. You can use ES client runner, cURL, or Postman.
    3. Create the `yce` index again, using ES client runner `createIndex` function or cURL ot Postman `POST` request.
    4. Close the `yce` index, using ES client runner `closeIndex` function or cURL or Postman `POST` request, so we can make settings and mapping changes.
    5. If necessary, add all settings needed (for analysis mostly), e.g., acronym analysis settings. You can use the ES client runner `putAcronymSettings` function, or use cURL or Postman to make the `PUT` request with the payload for all of your desired settings.
    6. Update (switch) the envinroments/indices in elasticsearch-dump and run the script to copy ONLY the mappings from `yce_backup` to `yce`.
    7. If necessary, add additional desired field mappings. For example, you can use the ES client runner `putTypeMappings` function, or use cURL or Postman to make the `PUT` request with the apyload for all of your desired mappings.
    8. Open the `yce` index, using ES client runner `openIndex` function or cURL or Postman `POST` request.
    9. Run elasticsearch-dump script and copy ONLY the data from `yce_backup` to `yce`. If you copied the mappings again here and you had added any additional mappigns previously, they would be overwritten and you'd need to start over from step 2.
    10. Run the ES client runner `scroll` function, in order to make any other updates or to add any additional fields to the documents in the index, such as lists of acronyms and lists of suggestiver-search keywords.
    11. Test out the index and make sure everything works correctly.


### TODO:
- Add documentation for autocomplete/predictive search
- Add documentation for boosting/weighting scores
- Add documentation for custom analyzers, tokenizers, and token filters
- Cleanup and add to Postman collections
- Add tests
